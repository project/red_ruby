<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php echo $language->language; ?>" xml:lang="<?php echo $language->language; ?>">
  <head>
    <title><?php if (isset($head_title)) echo $head_title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <?php echo $head; ?>
    <?php echo $styles ?>
    <?php echo $scripts ?>
    <!--[if IE 6]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie6.css" type="text/css" /><![endif]-->  
    <!--[if IE 7]><link rel="stylesheet" href="<?php echo $base_path . $directory; ?>/style.ie7.css" type="text/css" media="screen" /><![endif]-->
    <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  </head>

  <body<?php echo phptemplate_body_class($left, $right); ?>>
    <div class="Main">
      <div class="Header">
        <div class="fluid">
          <div class="logo">
            <a href="<?php echo $base_path; ?>" title="<?php echo t('Home'); ?>"><img src="<?php echo $logo; ?>" alt="<?php echo t('Home'); ?>" /></a>
          </div>
        </div>
      </div>
      <?php if (!empty($navigation)): ?>
        <div class="nav">
          <div class="l"></div>
          <div class="r"></div>
          <div class="fluid">
            <?php echo $navigation; ?>
          </div>
        </div>
      <?php endif; ?>
      <div id="container" class="fluid">
        <?php if ($left): ?>
          <div id="sidebar-left" class="sidebar">
            <?php echo $left; ?>
          </div>
        <?php endif; ?>
        <div id="main">
          <div class="content-padding">
            <?php if (!$is_front && $breadcrumb): echo theme('breadcrumb', $breadcrumb); endif; ?>
            <?php if ($tabs): echo '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
            <?php if ($title): echo '<h2 class="PostHeaderIcon-wrapper'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif; ?>
            <?php if ($tabs): echo $tabs . '</div>'; endif; ?>
            <?php if ($tabs2): echo '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
            <?php if ($mission): echo '<div id="mission">' . $mission . '</div>'; endif; ?>
            <?php if ($help): echo $help; endif; ?>
            <?php if ($show_messages && $messages): echo $messages; endif; ?>
            <?php echo art_content_replace($content); ?>
          </div>
        </div>
        <?php if ($right): ?>
          <div id="sidebar-right" class="sidebar">
            <?php echo $right; ?>
          </div>
        <?php endif; ?>
      </div>
      <div class="cleared"></div>
      <div class="Footer">
        <div class="Footer-inner">
          <div class="fluid">
            <a href="<?php echo $base_url; ?>/rss.xml" class="rss-tag-icon" title="RSS"></a>
            <div class="fluid">
              <?php if (!empty($footer)) echo $footer; ?>
            </div>
          </div>
        </div>
        <div class="Footer-background"></div>
      </div>
      <div class="cleared"></div>
      <p class="page-footer"><?php echo $footer_message; ?></p>
    </div>
    <?php echo $closure; ?>
  </body>
</html>